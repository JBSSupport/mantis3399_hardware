 //GC8034_tables.c
/*****************************************************************************/
/*
 *  \file        GC8034_tables.c \n
 *  \version     0.1.0 \n
 *	
 */
/*****************************************************************************/

#include <ebase/types.h>
#include <ebase/trace.h>
#include <ebase/builtins.h>

#include <common/return_codes.h>

#include "isi.h"
#include "isi_iss.h"
#include "isi_priv.h"
#include "GC8034_MIPI_priv.h"



/*****************************************************************************
 * GLOBALS
 *****************************************************************************/

const IsiRegDescription_t GC8034_g_aRegDescription_fourlane[] =
{
	/**/
	/*SYS*/

	{0xf2, 0x00,"0x0100",eReadWrite},
	{0xf4, 0x80,"0x0100",eReadWrite},
	{0xf5, 0x19,"0x0100",eReadWrite},
	{0xf6, 0x44,"0x0100",eReadWrite},
	{0xf8, 0x63,"0x0100",eReadWrite},
	{0xfa, 0x45,"0x0100",eReadWrite},
	{0xf9, 0x00,"0x0100",eReadWrite},
	{0xf7, 0x9d,"0x0100",eReadWrite},
	{0xfc, 0x00,"0x0100",eReadWrite},
	{0xfc, 0x00,"0x0100",eReadWrite},
	{0xfc, 0xea,"0x0100",eReadWrite},
	{0xfe, 0x03,"0x0100",eReadWrite},
	{0x03, 0x9a,"0x0100",eReadWrite},
	{0x18, 0x07,"0x0100",eReadWrite},
  	{0x01, 0x07,"0x0100",eReadWrite},
	{0xfc, 0xee,"0x0100",eReadWrite},
		/*Cisctl&Analog*/
	{0xfe, 0x00,"0x0100",eReadWrite},           
	{0x03, 0x08,"0x0100",eReadWrite},         	
	{0x04, 0xc6,"0x0100",eReadWrite},         	
	{0x05, 0x02,"0x0100",eReadWrite},         	
	{0x06, 0x16,"0x0100",eReadWrite},         	
	{0x07, 0x00,"0x0100",eReadWrite},         	
	{0x08, 0x10,"0x0100",eReadWrite},         	
	{0x0a, 0x3a,"0x0100",eReadWrite},         	
	{0x0b, 0x00,"0x0100",eReadWrite},         	
	{0x0c, 0x04,"0x0100",eReadWrite},         	
	{0x0d, 0x09,"0x0100",eReadWrite},         	
	{0x0e, 0xa0,"0x0100",eReadWrite},         	
	{0x0f, 0x0c,"0x0100",eReadWrite},         	
	{0x10, 0xd4,"0x0100",eReadWrite},         	
	{0x17, 0xc0,"0x0100",eReadWrite},	
	{0x18, 0x02,"0x0100",eReadWrite},         	
	{0x19, 0x17,"0x0100",eReadWrite},         	
	{0x1e, 0x50,"0x0100",eReadWrite},         	
	{0x1f, 0x80,"0x0100",eReadWrite},         	
	{0x21, 0x4c,"0x0100",eReadWrite},         	
	{0x25, 0x00,"0x0100",eReadWrite},         	
	{0x28, 0x4a,"0x0100",eReadWrite},         	
	{0x2d, 0x89,"0x0100",eReadWrite},         	
	{0xca, 0x02,"0x0100",eReadWrite},         	
	{0xcb, 0x00,"0x0100",eReadWrite},         	
	{0xcc, 0x39,"0x0100",eReadWrite},         	
	{0xce, 0xd0,"0x0100",eReadWrite},         	
	{0xcf, 0x93,"0x0100",eReadWrite},         	
	{0xd0, 0x19,"0x0100",eReadWrite},         	
	{0xd1, 0xaa,"0x0100",eReadWrite},         	
	{0xd2, 0xcb,"0x0100",eReadWrite},         	
	{0xd8, 0x40,"0x0100",eReadWrite},         	
	{0xd9, 0xff,"0x0100",eReadWrite},         	
	{0xda, 0x0e,"0x0100",eReadWrite},         	
	{0xdb, 0xb0,"0x0100",eReadWrite},         	
	{0xdc, 0x0e,"0x0100",eReadWrite},         	
	{0xde, 0x08,"0x0100",eReadWrite},         	
	{0xe4, 0xc6,"0x0100",eReadWrite},         	
	{0xe5, 0x08,"0x0100",eReadWrite},         	
	{0xe6, 0x10,"0x0100",eReadWrite},         	
	{0xed, 0x2a,"0x0100",eReadWrite},         	
	{0xfe, 0x02,"0x0100",eReadWrite},         	
	{0x59, 0x02,"0x0100",eReadWrite},         	
	{0x5a, 0x04,"0x0100",eReadWrite},         	
	{0x5b, 0x08,"0x0100",eReadWrite},         	
	{0x5c, 0x20,"0x0100",eReadWrite},         
	{0xfe, 0x00,"0x0100",eReadWrite},         
	{0x1a, 0x09,"0x0100",eReadWrite},         
	{0x1d, 0x13,"0x0100",eReadWrite},         
	{0xfe, 0x10,"0x0100",eReadWrite},         
	{0xfe, 0x00,"0x0100",eReadWrite},         
	{0xfe, 0x10,"0x0100",eReadWrite},         
	{0xfe, 0x00,"0x0100",eReadWrite},	       

	/*Gamma*/

	{0xfe, 0x00,"0x0100",eReadWrite},  
	{0x20, 0x55,"0x0100",eReadWrite},
	{0x33, 0x83,"0x0100",eReadWrite},
	{0xfe, 0x01,"0x0100",eReadWrite},
	{0xdf, 0x06,"0x0100",eReadWrite},
	{0xe7, 0x18,"0x0100",eReadWrite},
	{0xe8, 0x20,"0x0100",eReadWrite},
	{0xe9, 0x16,"0x0100",eReadWrite},
	{0xea, 0x17,"0x0100",eReadWrite},
	{0xeb, 0x50,"0x0100",eReadWrite},
	{0xec, 0x6c,"0x0100",eReadWrite},
	{0xed, 0x9b,"0x0100",eReadWrite},
	{0xee, 0xd8,"0x0100",eReadWrite},
	

	/*ISP*/

	{0xfe, 0x00,"0x0100",eReadWrite},  
	{0x80, 0x10,"0x0100",eReadWrite},
	{0x84, 0x01,"0x0100",eReadWrite},
	{0x88, 0x03,"0x0100",eReadWrite},
	{0x89, 0x03,"0x0100",eReadWrite},
	{0x8d, 0x03,"0x0100",eReadWrite},
	{0x8f, 0x14,"0x0100",eReadWrite},
	{0xad, 0x30,"0x0100",eReadWrite},
  	{0x66, 0x2c,"0x0100",eReadWrite},
	{0xbc, 0x49,"0x0100",eReadWrite},
	{0xc2, 0x7f,"0x0100",eReadWrite},
	{0xc3, 0xff,"0x0100",eReadWrite},
	
	
	
	
	/*Crop window*/

	{0x90, 0x01,"0x0100",eReadWrite},       
	{0x92,0x08,"0x0100",eReadWrite}, //crop y
	{0x94,0x09,"0x0100",eReadWrite}, //crop x
	{0x95, 0x04,"0x0100",eReadWrite},     
	{0x96, 0xc8,"0x0100",eReadWrite},     
	{0x97, 0x06,"0x0100",eReadWrite},     
	{0x98, 0x60,"0x0100",eReadWrite},     

	/*Gain*/

	{0xb0,0x90,"0x0100",eReadWrite},
	{0xb1,0x01,"0x0100",eReadWrite},
	{0xb2,0x00,"0x0100",eReadWrite},
	{0xb6,0x00,"0x0100",eReadWrite},

	/*BLK*/

	{0xfe, 0x00,"0x0100",eReadWrite},  
	{0x40, 0x22,"0x0100",eReadWrite},
	{0x41, 0x20,"0x0100",eReadWrite},
	{0x42, 0x02,"0x0100",eReadWrite},
	{0x43, 0x08,"0x0100",eReadWrite},
	{0x4e, 0x0f,"0x0100",eReadWrite},
	{0x4f, 0xf0,"0x0100",eReadWrite},
	{0x58, 0x80,"0x0100",eReadWrite},
	{0x59, 0x80,"0x0100",eReadWrite},
	{0x5a, 0x80,"0x0100",eReadWrite},
	{0x5b, 0x80,"0x0100",eReadWrite},
	{0x5c, 0x00,"0x0100",eReadWrite},
	{0x5d, 0x00,"0x0100",eReadWrite},
  	{0x5e, 0x00,"0x0100",eReadWrite},
	{0x5f, 0x00,"0x0100",eReadWrite},
	{0x6b, 0x01,"0x0100",eReadWrite},
	{0x6c, 0x00,"0x0100",eReadWrite},
	{0x6d, 0x0c,"0x0100",eReadWrite},
	
	/*WB offset*/

	{0xfe,0x01,"0x0100",eReadWrite},
	{0xbf,0x40,"0x0100",eReadWrite},

	/*Dark Sun*/

	{0xfe,0x01,"0x0100",eReadWrite},
	{0x68,0x77,"0x0100",eReadWrite},

	/*DPC*/

	{0xfe, 0x01,"0x0100",eReadWrite},  
	{0x60, 0x00,"0x0100",eReadWrite},
	{0x61, 0x10,"0x0100",eReadWrite},
	{0x62, 0x28,"0x0100",eReadWrite},
	{0x63, 0x10,"0x0100",eReadWrite},
	{0x64, 0x02,"0x0100",eReadWrite},

	/*LSC*/

	{0xfe, 0x01,"0x0100",eReadWrite},  
	{0xa8, 0x60,"0x0100",eReadWrite},
	{0xa2, 0xd1,"0x0100",eReadWrite},
	{0xc8, 0x57,"0x0100",eReadWrite},
	{0xa1, 0xb8,"0x0100",eReadWrite},
	{0xa3, 0x91,"0x0100",eReadWrite},
	{0xc0, 0x50,"0x0100",eReadWrite},
	{0xd0, 0x05,"0x0100",eReadWrite},
	{0xd1, 0xb2,"0x0100",eReadWrite},
	{0xd2, 0x1f,"0x0100",eReadWrite},
	{0xd3, 0x00,"0x0100",eReadWrite},
	{0xd4, 0x00,"0x0100",eReadWrite},
	{0xd5, 0x00,"0x0100",eReadWrite},
	{0xd6, 0x00,"0x0100",eReadWrite},
	{0xd7, 0x00,"0x0100",eReadWrite},
	{0xd8, 0x00,"0x0100",eReadWrite},
	{0xd9, 0x00,"0x0100",eReadWrite},
	{0xa4, 0x10,"0x0100",eReadWrite},
	{0xa5, 0x20,"0x0100",eReadWrite},
	{0xa6, 0x60,"0x0100",eReadWrite},
	{0xa7, 0x80,"0x0100",eReadWrite},
	{0xab, 0x18,"0x0100",eReadWrite},
	{0xc7, 0xc0,"0x0100",eReadWrite},

	/*ABB*/

	{0xfe, 0x01,"0x0100",eReadWrite},  
	{0x20, 0x02,"0x0100",eReadWrite},
	{0x21, 0x02,"0x0100",eReadWrite},
	{0x23, 0x42,"0x0100",eReadWrite},

	/*MIPI*/

	{0xfe, 0x03,"0x0100",eReadWrite},  
	{0x02, 0x03,"0x0100",eReadWrite},
	{0x04, 0x80,"0x0100",eReadWrite},
	{0x11, 0x2b,"0x0100",eReadWrite},
	{0x12, 0xf8,"0x0100",eReadWrite},
	{0x13, 0x07,"0x0100",eReadWrite},
	{0x15, 0x10,"0x0100",eReadWrite},
	{0x16, 0x29,"0x0100",eReadWrite},
	{0x17, 0xff,"0x0100",eReadWrite},
	{0x19, 0xaa,"0x0100",eReadWrite},
	{0x1a, 0x02,"0x0100",eReadWrite},
	{0x21, 0x02,"0x0100",eReadWrite},
	{0x22, 0x03,"0x0100",eReadWrite},
	{0x23, 0x0a,"0x0100",eReadWrite},
	{0x24, 0x00,"0x0100",eReadWrite},
	{0x25, 0x12,"0x0100",eReadWrite},
	{0x26, 0x04,"0x0100",eReadWrite},
	{0x29, 0x04,"0x0100",eReadWrite},
	{0x2a, 0x02,"0x0100",eReadWrite},
	{0x2b, 0x04,"0x0100",eReadWrite},
	{0xfe, 0x00,"0x0100",eReadWrite},
	{0x3f, 0x00,"0x0100",eReadWrite},

	{0x00 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t GC8034_g_3264x2448_fourlane[] =
{
	/* SYS */
	{0xf2, 0x00,"0x0100",eReadWrite},  
	{0xf4, 0x80,"0x0100",eReadWrite},
	{0xf5, 0x19,"0x0100",eReadWrite},
	{0xf6, 0x44,"0x0100",eReadWrite},
	{0xf8, 0x63,"0x0100",eReadWrite},
	{0xfa, 0x45,"0x0100",eReadWrite},
	{0xf9, 0x00,"0x0100",eReadWrite},
	{0xf7, 0x95,"0x0100",eReadWrite},
	{0xfc, 0x00,"0x0100",eReadWrite},
	{0xfc, 0x00,"0x0100",eReadWrite},
	{0xfc, 0xea,"0x0100",eReadWrite},
	{0xfe, 0x03,"0x0100",eReadWrite},
	{0x03, 0x9a,"0x0100",eReadWrite},
	{0x18, 0x07,"0x0100",eReadWrite},
	{0x01, 0x07,"0x0100",eReadWrite},
	{0xfc, 0xee,"0x0100",eReadWrite},

	/* ISP */
	{0xfe, 0x00,"0x0100",eReadWrite},
	{0x80, 0x13,"0x0100",eReadWrite},
	{0xad, 0x00,"0x0100",eReadWrite},

	/* Crop window */
	{0x90, 0x01,"0x0100",eReadWrite},        
	{0x92, 0x08,"0x0100",eReadWrite},
	{0x94, 0x09,"0x0100",eReadWrite},
	{0x95, 0x09,"0x0100",eReadWrite},      
	{0x96, 0x90,"0x0100",eReadWrite},      
	{0x97, 0x0c,"0x0100",eReadWrite},      
	{0x98, 0xc0,"0x0100",eReadWrite},      

	/* DPC */
	{0xfe, 0x01,"0x0100",eReadWrite},
	{0x62, 0x60,"0x0100",eReadWrite},
	{0x63, 0x48,"0x0100",eReadWrite},

	/* MIPI */
	{0xfe, 0x03,"0x0100",eReadWrite},  
	{0x02, 0x03,"0x0100",eReadWrite},
	{0x04, 0x80,"0x0100",eReadWrite},
	{0x11, 0x2b,"0x0100",eReadWrite},
	{0x12, 0xf0,"0x0100",eReadWrite},
	{0x13, 0x0f,"0x0100",eReadWrite},
	{0x15, 0x10,"0x0100",eReadWrite},
	{0x16, 0x29,"0x0100",eReadWrite},
	{0x17, 0xff,"0x0100",eReadWrite},
	{0x19, 0xaa,"0x0100",eReadWrite},
	{0x1a, 0x02,"0x0100",eReadWrite},
	{0x21, 0x05,"0x0100",eReadWrite},
	{0x22, 0x06,"0x0100",eReadWrite},
	{0x23, 0x2b,"0x0100",eReadWrite},
	{0x24, 0x00,"0x0100",eReadWrite},
	{0x25, 0x12,"0x0100",eReadWrite},
	{0x26, 0x07,"0x0100",eReadWrite},
	{0x29, 0x07,"0x0100",eReadWrite},
	{0x2a, 0x12,"0x0100",eReadWrite},
	{0x2b, 0x07,"0x0100",eReadWrite},
	{0xfe, 0x00,"0x0100",eReadWrite},
	{0x3f, 0xd0,"0x0100",eReadWrite},
	{0x00 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t GC8034_g_3264x2448P7_fourlane_fpschg[] =
{
	{0xfe, 0x00,"0x0100",eReadWrite},
  {0x07, 0x20,"0x0100",eReadWrite}, // VB H							 
	{0x08, 0x28,"0x0100",eReadWrite}, // VB L	
	
	{0x00 ,0x00,"eTableEnd",eTableEnd}
};
const IsiRegDescription_t GC8034_g_3264x2448P10_fourlane_fpschg[] =
{
	{0xfe, 0x00,"0x0100",eReadWrite},
  {0x07, 0x13,"0x0100",eReadWrite}, // VB H							 
	{0x08, 0x9E,"0x0100",eReadWrite}, // VB L	
	
	{0x00 ,0x00,"eTableEnd",eTableEnd}
};
const IsiRegDescription_t GC8034_g_3264x2448P15_fourlane_fpschg[] =
{
	{0xfe, 0x00,"0x0100",eReadWrite},
  {0x07, 0x09,"0x0100",eReadWrite}, // VB H							 
	{0x08, 0xDD,"0x0100",eReadWrite}, // VB L
	
	{0x00 ,0x00,"eTableEnd",eTableEnd}
};
const IsiRegDescription_t GC8034_g_3264x2448P20_fourlane_fpschg[] =
{
	{0xfe, 0x00,"0x0100",eReadWrite},
  {0x07, 0x04,"0x0100",eReadWrite}, // VB H							 
	{0x08, 0xFD,"0x0100",eReadWrite}, // VB L
	
	{0x00 ,0x00,"eTableEnd",eTableEnd}
};
const IsiRegDescription_t GC8034_g_3264x2448P25_fourlane_fpschg[] =
{
	{0xfe, 0x00,"0x0100",eReadWrite},
	{0x07, 0x02,"0x0100",eReadWrite}, // VB H							 
	{0x08, 0x10,"0x0100",eReadWrite}, // VB L
	
	{0x00 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t GC8034_g_3264x2448P30_fourlane_fpschg[] =
{
	{0xfe, 0x00,"0x0100",eReadWrite},
	{0x07, 0x00,"0x0100",eReadWrite}, // VB H							 
	{0x08, 0x1C,"0x0100",eReadWrite}, // VB L
	
	{0x00 ,0x00,"eTableEnd",eTableEnd}

};

